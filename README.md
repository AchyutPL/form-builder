![pageform_thumbnail](https://github.com/Kliton/yt_pageform/assets/10452377/610b5935-5afd-4126-9dfd-a7064e18a0db)

- Responsive
- Create forms with a stunning drag and drop designer
- Layout fields: Title, SubTitle, Spacer, Separator, Paragraph
- Form fields: Text, Number, Select, Date, Checkbox, Textarea
- Is easy to add and customize new fields
- Form preview dialog
- Share form url
- Form submission/validation
- Form stats: visits and submissions
